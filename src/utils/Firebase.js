// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getFirestore} from "firebase/firestore";


// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAU6B7rW6Ug4Mn2sym5kUPJQtLYO_P_jOI",
  authDomain: "prueba-96064.firebaseapp.com",
  projectId: "prueba-96064",
  storageBucket: "prueba-96064.appspot.com",
  messagingSenderId: "66102250346",
  appId: "1:66102250346:web:efa69fa2c0ac472bf57313"
};

// Initialize Firebase
export const initFirebase  = initializeApp(firebaseConfig);

export const db = getFirestore(initFirebase);
