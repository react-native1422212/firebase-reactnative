import React, { useEffect, useState } from 'react';
import { View, Text, FlatList } from 'react-native';
import { db } from '../utils';
import { collection, onSnapshot, query } from 'firebase/firestore';


export default function HomeScreen() {
  //estados iniciales del data.
  const [data, setData] = useState(null);
  
  useEffect(() => {
    const q = query(collection(db, "puntos_limpios")
    );

    onSnapshot(q, (snapshot) => { 
      setData(snapshot.docs);

      console.log(snapshot.docs);
    })
    console.log("hola mundo");
   
  })



  function renderItem({ item }) {
    const punto_limpio = item.data();
    return (
      <View style={{ margin: 10, padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        borderRadius: 8,
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.3,
        shadowRadius: 8, }}>
        <Text style={{fontWeight: 'bold'}}>{punto_limpio.nombre}</Text>
        <Text>Id: {punto_limpio.id}</Text>
        <Text>Location:</Text>
        <Text>Latitud: {punto_limpio.location.latitud}</Text>
        <Text>Longitud: {punto_limpio.location.longitud}</Text>
      </View>
    )
  }


  return (
    <View style={{ padding: 10, backgroundColor:'#374745' }}>
      <Text style={{fontStyle: 'italic'}}>Listado de centros comunitarios</Text>
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </View>
  )
}